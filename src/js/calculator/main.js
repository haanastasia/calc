import { journal, closeJournal } from './core/journal';
import { addition, subtraction, mod, division, multiplication, sqry, sqrtN, factorial, log, square, squareRoot, percentage, fraction, changesign } from './helpers/calculation'; import { updateMainScreen, updateHistoryScreen } from './core/screen';

export default class Calculator {

    constructor(name) {

        this.name = document.querySelector(name);
        // result
        this.result;
        // current number
        this.currNum;
        // prev result
        this.prevResult;
        // history
        this.history;
        // prev btn pressed
        this.prevBtn;
        // math operation
        this.mathOp;
        // prev math operation
        this.prevMathOp;
        // math operation counter
        this.mathOpCount;
        // math op pressed?
        this.mathOpPress;
        // init
        this.isInit;
        // main screen
        this.mainScreen = this.name.querySelector('#main');
        // history screen
        this.historyScreen = this.name.querySelector('#history');

        this.journal;

        const self = this;

        Array.prototype.forEach.call(this.name.querySelectorAll('.calculator__button'), function (btn) {
            btn.addEventListener('click', function (e) {
                var btnClicked = e.currentTarget.getAttribute('data-value');
                input(btnClicked);
            });
        });

        Array.prototype.forEach.call(this.name.querySelectorAll('.calculator__icon--journal'), function (btn) {
            btn.addEventListener('click', function (e) {
                journal(self.name);
            });
        });

        Array.prototype.forEach.call(this.name.querySelectorAll('.journal__trash'), function (btn) {
            btn.addEventListener('click', function (e) {
                trash(self.name);
            });
        });

        function trash(name) {
            name.querySelector('.journal__text').innerHTML = "Empty journal";
            self.journal = 0;
        }

        function input(btn) {

            if (!isNaN(self.prevBtn) && btn !== '=' && btn !== 'C' && btn !== 'CE' && btn !== 'CS' && btn !== '.' && btn !== '+-') {
                self.prevMathOp = self.mathOp;
            }

            switch (btn) {
                case '+': self.mathOpPress = true; self.mathOp = addition; break;
                case '-': self.mathOpPress = true; self.mathOp = subtraction; break;
                case '/': self.mathOpPress = true; self.mathOp = division; break;
                case '*': self.mathOpPress = true; self.mathOp = multiplication; break;
                case 'mod': self.mathOpPress = true; self.mathOp = mod; break;
                case '^': self.mathOpPress = true; self.mathOp = sqry; break;
                case '^√': self.mathOpPress = true; self.mathOp = sqrtN; break;
                case 'C': self.init(); break;
                case 'C': self.init(); break;
            }

            self.handler(btn);
        }

    }

    init() {

        this.result = null;
        this.currNum = 0;
        this.prevBtn = null;
        this.mathOp = null;
        this.prevMathOp = null;
        this.mathOpCount = 0;
        this.history = '';
        this.mathOpPress = false;
        this.isInit = true;
        updateMainScreen(this.name, 0);
        updateHistoryScreen(this.name, this.history);
        this.journal = 0;

    }

    handler(btn) {

        if (btn == 'C') {
            return;
        }

        // update history
        if (btn !== '=' && btn !== 'C' && btn !== 'CE' && btn !== 'CS' && btn !== '+-') {
            this.history = (isNaN(this.prevBtn) && isNaN(btn)) ? this.history.slice(0, -1) + btn : this.history + btn;
        }

        // change of sign +/-
        if (btn === '+-') {
            this.currNum = changesign(this.currNum);
            this.mathOpPress = false;
            updateMainScreen(this.name, this.currNum);
        }

        // is not an integer
        if (!isNaN(btn) || btn === '.') {
            if (btn === '.' && /^\d+$/.test(this.currNum)) {
                this.currNum = this.currNum + btn;
            } else if (!isNaN(btn)) {
                this.currNum = (!isNaN(this.prevBtn) && this.prevBtn !== null && this.mainScreen.value !== '0') || this.prevBtn === '.' ? this.currNum + btn : btn;
            }
            this.mathOpPress = false;
            updateMainScreen(this.name, this.currNum);
        } else {
            // update history - mathematical operations
            if (btn === '-' || btn === '+' || btn === '*' || btn === '/' || btn === 'mod' || btn === 'sqry' || btn === '^√') {
                if ((this.prevBtn === null || this.prevBtn === '=') && !this.isInit) {
                    this.history = '0' + btn;
                    this.mathOpCount++;
                }
                if (!this.historyScreen.value.length && this.mainScreen.value.length) {
                    this.history = this.mainScreen.value + btn;
                }
            }

            // calculating the logarithm
            if (btn === '!n') {
                this.currNum = factorial(this.currNum); updateMainScreen(this.name, this.currNum); updateHistoryScreen(this.name, this.currNum);
            }

            // calculating the factorial
            if (btn === 'log') {
                this.currNum = log(this.currNum); updateMainScreen(this.name, this.currNum); updateMainScreen(this.name, this.currNum);
            }

            // if math op was pressed and result is null
            if (this.mathOp && this.result === null) {
                this.result = Number(this.currNum);
            }

            // count percents
            if (btn === '%') {
                this.history = this.history.slice(0, -(this.currNum.length + 1));
                this.currNum = percentage(this.currNum, this.result);
                this.history += this.currNum + ' ';
                updateMainScreen(this.name, this.currNum);
            } else if (btn === 'sqr' || btn === '√' || btn === '1/x') {
                this.history = this.history.slice(0, -(this.currNum.length + btn.length)) + (btn === '1/x' ? '1/(' + this.currNum + ') ' : btn + '(' + this.currNum + ') ');
                this.currNum = (btn === 'sqr') ? square(this.currNum) : (btn === '√') ? squareRoot(this.currNum) : fraction(this.currNum);
                updateMainScreen(this.name, this.currNum);
                updateHistoryScreen(this.name, this.history);
            }

            // calculating the result
            if (btn === '=') {

                if (this.mathOp) {
                    this.mathOpCount = 0;
                    if (this.mathOpPress) {
                        this.result = this.mathOp(this.result, this.prevResult);
                    } else {
                        this.result = this.mathOp(this.result, Number(this.currNum));
                    }
                    if (this.journal == 0) {
                        this.name.querySelector('.journal__text').innerHTML = '';
                    }
                    this.name.querySelector('.journal__text').innerHTML += this.history + '=' + this.result + '\n';
                    this.journal = 1;
                    this.history = '';
                    this.prevBtn = btn;
                    updateMainScreen(this.name, this.result);
                    updateHistoryScreen(this.name, this.history);
                    return;
                }
            }

            // if sign was pressed and prev btn isn't sign and except some buttons
            if (isNaN(btn) && (!isNaN(this.prevBtn) || this.prevBtn === '%' || this.prevBtn === 'sqr' || this.prevBtn === '√' || this.prevBtn === '1/x') &&
                btn !== '=' && btn !== 'C' && btn !== 'CE' && btn !== 'CS' && btn !== '.' && btn !== '%' && btn !== 'sqr' & btn !== '√' && btn !== '1/x') {
                this.mathOpCount++;
            }

            // count result in row
            if (this.mathOpCount >= 2 && (!isNaN(this.prevBtn) || this.prevBtn === '√' || this.prevBtn === 'sqr' || this.prevBtn === '1/x' || this.prevBtn === '%') && btn !== 'CE' && btn !== 'CS' && btn !== '+-') {
                this.prevMathOp(Number(this.currNum));
                updateMainScreen(this.name, this.result);
            }

            // clear the whole result
            if (btn === 'CE' && this.history.length > 0) {
                this.history = this.history.slice(0, -(this.currNum.length));
                this.currNum = '0';
                updateMainScreen(this.name, 0);
            } else if (btn === 'CS') { // delete one character
                if (this.result != this.mainScreen.value) {
                    this.currNum = this.currNum.slice(0, -1);
                    this.history = this.history.slice(0, -1);
                    if (!this.currNum.length) {
                        this.currNum = '0';
                    }
                    updateMainScreen(this.name, this.currNum);
                } else {
                    return;
                }
            }

            if (this.result !== null && btn !== 'CE' && btn !== 'CS') {
                updateHistoryScreen(this.name, this.history);
            }

        }

        this.prevBtn = btn;
        this.prevResult = this.result;
        this.isInit = false;
    }

}