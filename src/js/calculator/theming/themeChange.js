// меняем цветовую гамму и тип калькулятора
export function themeChange(e) {

    // находим родителя для меню. 
    var parentsList = e.currentTarget.closest(".calculator-list__sub-list");

    for (var i = 0; i < parentsList.childNodes.length; i++) {
        var par = parentsList.childNodes[i];
        for (var t = 0; t < par.childNodes.length; t++) {
            par.childNodes[t].classList.remove("calculator-list__sub-element--active");
        }
    }

    e.currentTarget.classList.add("calculator-list__sub-element--active");
    var el = e.currentTarget.getAttribute('data-value');

    // находим калькулятор.
    var parentsCalc = e.currentTarget.closest(".calculator");

    if (el === 'standart') {
        parentsCalc.querySelector('#name').innerHTML = "Standart";
        var els = parentsCalc.querySelectorAll('.calculator__row--scientific');
        for (var i = 0; i < els.length; i++) {
            els[i].classList.add("calculator__row--none");
            els[i].classList.remove("calculator__row--active");
        }
    }

    if (el === 'scientific') {
        parentsCalc.querySelector('#name').innerHTML = "Scientific";
        var els = parentsCalc.querySelectorAll('.calculator__row--scientific');
        for (var i = 0; i < els.length; i++) {
            els[i].classList.add("calculator__row--active");
            els[i].classList.remove("calculator__row--none");
        }
    }

    if (el === 'light') {
        parentsCalc.classList.add("calculator--light");
        parentsCalc.classList.remove("calculator--dark");
    }

    if (el === 'dark') {
        parentsCalc.classList.add("calculator--dark");
        parentsCalc.classList.remove("calculator--light");
    }

}