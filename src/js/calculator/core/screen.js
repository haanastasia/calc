// update the screen with the result
export function updateMainScreen(name, val) {
    val = String(val);
    name.querySelector('#main').value = val;
}

// updating the history with calculations
export function updateHistoryScreen(name, history) {
    name.querySelector('#history').value = history;
}