export function journal(name) {
    name.querySelector('.journal').classList.toggle("journal--active");
    closeJournal(name);
}

export function closeJournal(name) {
    window.addEventListener('click', function (e) {
        if (!name.querySelector('.journal').contains(e.target) & !name.querySelector('.calculator__icon--journal').contains(e.target)) {
            name.querySelector('.journal').classList.remove("journal--active");
        }
    });
}
