import { themeChange } from '../theming/themeChange';

export default class CalculatorMenu {

    constructor(name) {
        const self = this;
    }

    init() {

        Array.prototype.forEach.call(document.querySelectorAll('.btn-menu'), function (el) {
            el.addEventListener('click', function (e) {
                menu(e.currentTarget);
            });
        });

        Array.prototype.forEach.call(document.querySelectorAll('.calculator-list__sub-element'), function (el) {
            el.addEventListener('click', function (e) {
                themeChange(e);
            });
        });

        function menu(e) {
            e.classList.toggle("btn-menu--active");
            closeMenu(e);
        }

        // закрываем меню, если клик произошел за пределами контейнера
        function closeMenu(parents) {
            window.addEventListener('click', function (e) {
                if (!parents.nextElementSibling.contains(e.target) & !parents.contains(e.target)) {
                    parents.classList.remove("btn-menu--active");
                }
            });
        }

    }

}

