// mathematical operations
export function addition(result, val) {
    return result + val;
}

export function subtraction(result, val) {
    return result - val;
}

export function mod(result, val) {
    return result % val;
}

export function division(result, val) {
    return result / val;
}

export function multiplication(result, val) {
    return result * val;
}

export function sqry(result, val) {
    return Math.pow(result, val);
}

export function sqrtN(result, val) {
    return Math.pow(result, 1 / val);
}

export function factorial(val) {
    return (val != 1) ? val * factorial(val - 1) : 1;
}

export function log(val) {
    return Math.log(val);
}

export function square(val) {
    return val * val;
}

export function squareRoot(val) {
    return Math.sqrt(val);
}

export function percentage(val, res) {
    return res * val / 100;
}

export function fraction(val) {
    return 1 / val;
}

export function changesign(val) {
    return val * -1;
}