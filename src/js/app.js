'use strict';

import CalculatorMenu from './calculator/core/menu';

let calcMenu = new CalculatorMenu();
calcMenu.init();

import Calculator from './calculator/main';

let calc1 = new Calculator("#calcFirst");
let calc2 = new Calculator("#calcSecond");

calc1.init();
calc2.init();

import '../sass/styles.scss';

function importAll(r) {
    return r.keys().map(r);
}

const images = importAll(require.context('../img/', true, /\.(png|jpe?g|svg)$/));