/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./src/js/calculator/theming/themeChange.js
// меняем цветовую гамму и тип калькулятора
function themeChange(e) {

    // находим родителя для меню. 
    var parentsList = e.currentTarget.closest(".calculator-list__sub-list");

    for (var i = 0; i < parentsList.childNodes.length; i++) {
        var par = parentsList.childNodes[i];
        for (var t = 0; t < par.childNodes.length; t++) {
            par.childNodes[t].classList.remove("calculator-list__sub-element--active");
        }
    }

    e.currentTarget.classList.add("calculator-list__sub-element--active");
    var el = e.currentTarget.getAttribute('data-value');

    // находим калькулятор.
    var parentsCalc = e.currentTarget.closest(".calculator");

    if (el === 'standart') {
        parentsCalc.querySelector('#name').innerHTML = "Standart";
        var els = parentsCalc.querySelectorAll('.calculator__row--scientific');
        for (var i = 0; i < els.length; i++) {
            els[i].classList.add("calculator__row--none");
            els[i].classList.remove("calculator__row--active");
        }
    }

    if (el === 'scientific') {
        parentsCalc.querySelector('#name').innerHTML = "Scientific";
        var els = parentsCalc.querySelectorAll('.calculator__row--scientific');
        for (var i = 0; i < els.length; i++) {
            els[i].classList.add("calculator__row--active");
            els[i].classList.remove("calculator__row--none");
        }
    }

    if (el === 'light') {
        parentsCalc.classList.add("calculator--light");
        parentsCalc.classList.remove("calculator--dark");
    }

    if (el === 'dark') {
        parentsCalc.classList.add("calculator--dark");
        parentsCalc.classList.remove("calculator--light");
    }
}
// CONCATENATED MODULE: ./src/js/calculator/core/menu.js


class menu_CalculatorMenu {

    constructor(name) {
        const self = this;
    }

    init() {

        Array.prototype.forEach.call(document.querySelectorAll('.btn-menu'), function (el) {
            el.addEventListener('click', function (e) {
                menu(e.currentTarget);
            });
        });

        Array.prototype.forEach.call(document.querySelectorAll('.calculator-list__sub-element'), function (el) {
            el.addEventListener('click', function (e) {
                themeChange(e);
            });
        });

        function menu(e) {
            e.classList.toggle("btn-menu--active");
            closeMenu(e);
        }

        // закрываем меню, если клик произошел за пределами контейнера
        function closeMenu(parents) {
            window.addEventListener('click', function (e) {
                if (!parents.nextElementSibling.contains(e.target) & !parents.contains(e.target)) {
                    parents.classList.remove("btn-menu--active");
                }
            });
        }
    }

}
// CONCATENATED MODULE: ./src/js/calculator/core/journal.js
function journal(name) {
    name.querySelector('.journal').classList.toggle("journal--active");
    closeJournal(name);
}

function closeJournal(name) {
    window.addEventListener('click', function (e) {
        if (!name.querySelector('.journal').contains(e.target) & !name.querySelector('.calculator__icon--journal').contains(e.target)) {
            name.querySelector('.journal').classList.remove("journal--active");
        }
    });
}
// CONCATENATED MODULE: ./src/js/calculator/helpers/calculation.js
// mathematical operations
function addition(result, val) {
    return result + val;
}

function subtraction(result, val) {
    return result - val;
}

function mod(result, val) {
    return result % val;
}

function division(result, val) {
    return result / val;
}

function multiplication(result, val) {
    return result * val;
}

function sqry(result, val) {
    return Math.pow(result, val);
}

function sqrtN(result, val) {
    return Math.pow(result, 1 / val);
}

function factorial(val) {
    return val != 1 ? val * factorial(val - 1) : 1;
}

function log(val) {
    return Math.log(val);
}

function square(val) {
    return val * val;
}

function squareRoot(val) {
    return Math.sqrt(val);
}

function percentage(val, res) {
    return res * val / 100;
}

function fraction(val) {
    return 1 / val;
}

function changesign(val) {
    return val * -1;
}
// CONCATENATED MODULE: ./src/js/calculator/core/screen.js
// update the screen with the result
function updateMainScreen(name, val) {
    val = String(val);
    name.querySelector('#main').value = val;
}

// updating the history with calculations
function updateHistoryScreen(name, history) {
    name.querySelector('#history').value = history;
}
// CONCATENATED MODULE: ./src/js/calculator/main.js



class main_Calculator {

    constructor(name) {

        this.name = document.querySelector(name);
        // result
        this.result;
        // current number
        this.currNum;
        // prev result
        this.prevResult;
        // history
        this.history;
        // prev btn pressed
        this.prevBtn;
        // math operation
        this.mathOp;
        // prev math operation
        this.prevMathOp;
        // math operation counter
        this.mathOpCount;
        // math op pressed?
        this.mathOpPress;
        // init
        this.isInit;
        // main screen
        this.mainScreen = this.name.querySelector('#main');
        // history screen
        this.historyScreen = this.name.querySelector('#history');

        this.journal;

        const self = this;

        Array.prototype.forEach.call(this.name.querySelectorAll('.calculator__button'), function (btn) {
            btn.addEventListener('click', function (e) {
                var btnClicked = e.currentTarget.getAttribute('data-value');
                input(btnClicked);
            });
        });

        Array.prototype.forEach.call(this.name.querySelectorAll('.calculator__icon--journal'), function (btn) {
            btn.addEventListener('click', function (e) {
                journal(self.name);
            });
        });

        Array.prototype.forEach.call(this.name.querySelectorAll('.journal__trash'), function (btn) {
            btn.addEventListener('click', function (e) {
                trash(self.name);
            });
        });

        function trash(name) {
            name.querySelector('.journal__text').innerHTML = "Empty journal";
            self.journal = 0;
        }

        function input(btn) {

            if (!isNaN(self.prevBtn) && btn !== '=' && btn !== 'C' && btn !== 'CE' && btn !== 'CS' && btn !== '.' && btn !== '+-') {
                self.prevMathOp = self.mathOp;
            }

            switch (btn) {
                case '+':
                    self.mathOpPress = true;self.mathOp = addition;break;
                case '-':
                    self.mathOpPress = true;self.mathOp = subtraction;break;
                case '/':
                    self.mathOpPress = true;self.mathOp = division;break;
                case '*':
                    self.mathOpPress = true;self.mathOp = multiplication;break;
                case 'mod':
                    self.mathOpPress = true;self.mathOp = mod;break;
                case '^':
                    self.mathOpPress = true;self.mathOp = sqry;break;
                case '^√':
                    self.mathOpPress = true;self.mathOp = sqrtN;break;
                case 'C':
                    self.init();break;
                case 'C':
                    self.init();break;
            }

            self.handler(btn);
        }
    }

    init() {

        this.result = null;
        this.currNum = 0;
        this.prevBtn = null;
        this.mathOp = null;
        this.prevMathOp = null;
        this.mathOpCount = 0;
        this.history = '';
        this.mathOpPress = false;
        this.isInit = true;
        updateMainScreen(this.name, 0);
        updateHistoryScreen(this.name, this.history);
        this.journal = 0;
    }

    handler(btn) {

        if (btn == 'C') {
            return;
        }

        // update history
        if (btn !== '=' && btn !== 'C' && btn !== 'CE' && btn !== 'CS' && btn !== '+-') {
            this.history = isNaN(this.prevBtn) && isNaN(btn) ? this.history.slice(0, -1) + btn : this.history + btn;
        }

        // change of sign +/-
        if (btn === '+-') {
            this.currNum = changesign(this.currNum);
            this.mathOpPress = false;
            updateMainScreen(this.name, this.currNum);
        }

        // is not an integer
        if (!isNaN(btn) || btn === '.') {
            if (btn === '.' && /^\d+$/.test(this.currNum)) {
                this.currNum = this.currNum + btn;
            } else if (!isNaN(btn)) {
                this.currNum = !isNaN(this.prevBtn) && this.prevBtn !== null && this.mainScreen.value !== '0' || this.prevBtn === '.' ? this.currNum + btn : btn;
            }
            this.mathOpPress = false;
            updateMainScreen(this.name, this.currNum);
        } else {
            // update history - mathematical operations
            if (btn === '-' || btn === '+' || btn === '*' || btn === '/' || btn === 'mod' || btn === 'sqry' || btn === '^√') {
                if ((this.prevBtn === null || this.prevBtn === '=') && !this.isInit) {
                    this.history = '0' + btn;
                    this.mathOpCount++;
                }
                if (!this.historyScreen.value.length && this.mainScreen.value.length) {
                    this.history = this.mainScreen.value + btn;
                }
            }

            // calculating the logarithm
            if (btn === '!n') {
                this.currNum = factorial(this.currNum);updateMainScreen(this.name, this.currNum);updateHistoryScreen(this.name, this.currNum);
            }

            // calculating the factorial
            if (btn === 'log') {
                this.currNum = log(this.currNum);updateMainScreen(this.name, this.currNum);updateMainScreen(this.name, this.currNum);
            }

            // if math op was pressed and result is null
            if (this.mathOp && this.result === null) {
                this.result = Number(this.currNum);
            }

            // count percents
            if (btn === '%') {
                this.history = this.history.slice(0, -(this.currNum.length + 1));
                this.currNum = percentage(this.currNum, this.result);
                this.history += this.currNum + ' ';
                updateMainScreen(this.name, this.currNum);
            } else if (btn === 'sqr' || btn === '√' || btn === '1/x') {
                this.history = this.history.slice(0, -(this.currNum.length + btn.length)) + (btn === '1/x' ? '1/(' + this.currNum + ') ' : btn + '(' + this.currNum + ') ');
                this.currNum = btn === 'sqr' ? square(this.currNum) : btn === '√' ? squareRoot(this.currNum) : fraction(this.currNum);
                updateMainScreen(this.name, this.currNum);
                updateHistoryScreen(this.name, this.history);
            }

            // calculating the result
            if (btn === '=') {

                if (this.mathOp) {
                    this.mathOpCount = 0;
                    if (this.mathOpPress) {
                        this.result = this.mathOp(this.result, this.prevResult);
                    } else {
                        this.result = this.mathOp(this.result, Number(this.currNum));
                    }
                    if (this.journal == 0) {
                        this.name.querySelector('.journal__text').innerHTML = '';
                    }
                    this.name.querySelector('.journal__text').innerHTML += this.history + '=' + this.result + '\n';
                    this.journal = 1;
                    this.history = '';
                    this.prevBtn = btn;
                    updateMainScreen(this.name, this.result);
                    updateHistoryScreen(this.name, this.history);
                    return;
                }
            }

            // if sign was pressed and prev btn isn't sign and except some buttons
            if (isNaN(btn) && (!isNaN(this.prevBtn) || this.prevBtn === '%' || this.prevBtn === 'sqr' || this.prevBtn === '√' || this.prevBtn === '1/x') && btn !== '=' && btn !== 'C' && btn !== 'CE' && btn !== 'CS' && btn !== '.' && btn !== '%' && btn !== 'sqr' & btn !== '√' && btn !== '1/x') {
                this.mathOpCount++;
            }

            // count result in row
            if (this.mathOpCount >= 2 && (!isNaN(this.prevBtn) || this.prevBtn === '√' || this.prevBtn === 'sqr' || this.prevBtn === '1/x' || this.prevBtn === '%') && btn !== 'CE' && btn !== 'CS' && btn !== '+-') {
                this.prevMathOp(Number(this.currNum));
                updateMainScreen(this.name, this.result);
            }

            // clear the whole result
            if (btn === 'CE' && this.history.length > 0) {
                this.history = this.history.slice(0, -this.currNum.length);
                this.currNum = '0';
                updateMainScreen(this.name, 0);
            } else if (btn === 'CS') {
                // delete one character
                if (this.result != this.mainScreen.value) {
                    this.currNum = this.currNum.slice(0, -1);
                    this.history = this.history.slice(0, -1);
                    if (!this.currNum.length) {
                        this.currNum = '0';
                    }
                    updateMainScreen(this.name, this.currNum);
                } else {
                    return;
                }
            }

            if (this.result !== null && btn !== 'CE' && btn !== 'CS') {
                updateHistoryScreen(this.name, this.history);
            }
        }

        this.prevBtn = btn;
        this.prevResult = this.result;
        this.isInit = false;
    }

}
// EXTERNAL MODULE: ./src/sass/styles.scss
var styles = __webpack_require__(15);

// CONCATENATED MODULE: ./src/js/app.js




let calcMenu = new menu_CalculatorMenu();
calcMenu.init();



let calc1 = new main_Calculator("#calcFirst");
let calc2 = new main_Calculator("#calcSecond");

calc1.init();
calc2.init();



function importAll(r) {
    return r.keys().map(r);
}

const app_images = importAll(__webpack_require__(12));

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/trash.png";

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/sqrt.png";

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/plus.png";

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/plus-minus.png";

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/percent.png";

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/multiply.png";

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/minus.png";

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/equal.png";

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/divide.png";

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/delete.png";

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/clock.png";

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./icon/clock.png": 11,
	"./icon/delete.png": 10,
	"./icon/divide.png": 9,
	"./icon/equal.png": 8,
	"./icon/minus.png": 7,
	"./icon/multiply.png": 6,
	"./icon/percent.png": 5,
	"./icon/plus-minus.png": 4,
	"./icon/plus.png": 3,
	"./icon/sqrt.png": 2,
	"./icon/trash.png": 1
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	var module = __webpack_require__(id);
	return module;
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 12;

/***/ }),
/* 13 */,
/* 14 */,
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })
/******/ ]);