# Calculator #

Simple calculator using SASS and ES6.

## About ##

### Patterns used ###
* Factory Method: /src/js/calculator/main.js
* Memoization: /src/js/calculator/main.js in line 179-182.
* Observer: /src/js/calculator/core/menu.js

## Getting Started ##

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites ###
* node.js or docker

### Installing vie webpack ###
* Download project ( git clone https://haanastasia@bitbucket.org/haanastasia/calc.git )
* Run node.js
* npm run build
* npm run start

### Installing vie docker ###
* Download project ( git clone git clone https://haanastasia@bitbucket.org/haanastasia/calc.git )
* Run docker
* docker build -t calculator .
* docker run -p 30000:30000 calculator
* open http://localhost:30000

## Built With ##
* Webpack
* Docker
* SASS
* ES6

## Changelog ##

### Added ###
* Journal
* Mathematical operations - nth root, exponentiation
* You can now create an unlimited number of calculators

### Changed ###
* Left menu - improved navigation
* Change of design
* Code architecture
* Update webpack to version 4.8.1
* Replaced module 'extract-text-webpack-plugin' with 'mini-css-extract-plugin'

### Removed ###
* Mathematical operations - exp, 3th root

## Authors ##

* Anastasia Dolgopolova (https://bitbucket.org/haanastasia/calc/)