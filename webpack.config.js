const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

var clientConfig = (function webpackConfig() {
    const PROD = process.env.NODE_ENV === 'production';
    const productionPath = 'dist/prod/';
    const developmentPath = 'dist/dev/';
    const publicPath = PROD ? productionPath : developmentPath;

    var config = Object.assign({});
    config.entry = './src/js/app.js';
    config.output = {
        path: path.resolve(__dirname, publicPath),
        filename: 'index.js'
    };

    config.optimization = {
        minimize: PROD ? true : false
    };

    config.module = {
        rules: [
            {
                test: /\.js$/,
                use: { loader: 'babel-loader' }
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            minimize: PROD ? true : false
                        }
                    },
                    'sass-loader'
                ]
            },
            {
                test: /\.(png|jpg|jpeg|gif|ico)$/,
                loader: 'file-loader',
                options: { name: 'img/[name].[ext]' }
            }
        ]
    };

    config.plugins = [
        new MiniCssExtractPlugin({
            filename: '[name].css'
        }),
        new HtmlWebpackPlugin({
            title: PROD ? 'Production mode' : 'Development mode',
            template: './src/index.html'
        }),
    ];

    return config;
});

module.exports = clientConfig;